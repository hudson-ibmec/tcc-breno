# **README**

Pasta com códigos utilizados para fazer a análise.

### **ESTRUTURA DESTA PASTA**

Nome do arquivo | Descrição
----------------|--------------------
[main.R](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/code/main.R) | Código central da análise. Ele roda os outros scripts.
[functions.R](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/code/functions.R) | Arquivo com funções criadas para a análise.
[webscraping.R](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/code/webscraping.R) | Script para o webscraping.
[sentiment_analysis.R](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/code/sentiment_analysis.R) | Script para a análise de sentimentos.
