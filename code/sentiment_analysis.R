
#####
##  Obtenção de palavras utilizadas na análise
#####

# Obter lista de palavras positivas, negativas e stopwords utilizadas na literatura (https://sraf.nd.edu/textual-analysis/resources/)
stop_words_long <- (sw_loughran_mcdonald_long)
stop_words_short <- (sw_loughran_mcdonald_short)
positive_words <- as.vector(hash_sentiment_loughran_mcdonald[y==1,1])
negative_words <- as.vector(hash_sentiment_loughran_mcdonald[y==-1,1])

# Obter lista de palavras amplificadoras, atenuadoras e de negação (Baixado da biblioteca qdap) TODO: ubuntu
negators <- read.csv("data/wordlist/negators.csv",stringsAsFactors = FALSE)[,2]
amplifiers <- read.csv("data/wordlist/amplifiers.csv",stringsAsFactors = FALSE)[,2]
deamplifiers <- read.csv("data/wordlist/deamplifiers.csv",stringsAsFactors = FALSE)[,2]





#####
##  PRÉ PROCESSAMENTO DO TEXTO E CONVERSÃO DO PDF
#####

# Ler os arquivos PDF das atas e transformar em um Corpus 
pdf_link_copom$text <- sapply(pdf_link_copom$n_ata, read_pdf) 
pdf_link_copom$text <- pdf_link_copom$text %>% as.character() 
print('Creating corpus')
docs <- VCorpus(VectorSource(pdf_link_copom$text))

#Pré processamento
print('Pre processing text')
docs = tm_map(docs, PlainTextDocument) # garantindo que o documento é um texto plano.

docs = tm_map(docs, content_transformer(tolower)) # covertendo todas as letras para minúsculas

docs = tm_map(docs, removeWords, stop_words_long) # removendo palavras que se repetem e não ajudam na análise

docs = tm_map(docs, toSpace, "/|@|\\|(|)") # removendo "/", "@" e "|"

#docs = tm_map(docs, toSpace, "the|\003|â€|\023") # removendo "the"

docs = tm_map(docs, removeNumbers) # remover números do texto.

docs = tm_map(docs, removePunctuation) # remover a pontuação. Fornece contexto gramatical que apoia o entendimento.

docs = tm_map(docs, stripWhitespace) # remover espaços em branco

# docs = tm_map(docs, stemDocument, language = "english") # deixar só o radical (Não utilizado)

#####
##  Análise exploratória (Não utilizada ainda no paper ) TODO: Criar tabela de termos frequentes daqui
#####

# # É possível remover os termos esparsos com a função "removeSparseTerms()". Precisamos especificar um número entre
# # zero e um onde quanto maior o número, maior o percentual de "Sparsity" na matriz. Assim, com 157 documentos, 
# # especificando 0,95 como o número de "sparsity", a matriz resultante deveria ter palavras que ocorreram ao menos
# # em 150 documentos, como segue:
# 
# dtm2 = removeSparseTerms(dtm, 0.90)
# 
# # encontrar as palavras que ocorrem pelo menos 400 vezes
# table = findFreqTerms(dtm2, 400)
# 
# # extrair o formato latex para inserir no artigo
# table = matrix(table[1:150], nrow = 30, ncol = 5, byrow = TRUE)
# # xtable(table) 
# 
# # encontrar a associação de uma palavra com demais
# #findAssocs(dtm, "behavior", corlimit = 0.5)
# 
# freq = colSums(as.matrix(dtm2))
# 
# wordcloud(names(freq), freq, min.freq = 100, scale = c(3, .5), colors = brewer.pal(6, "Dark2"), max.words = 30)

#####
##  PROCESSAR O LEXICON 
#####

## 1) Unigram

print('Creating unigram score')

# Criando DTM
dtm = DocumentTermMatrix(docs) 
dimnames(dtm)$Docs = pdf_link_copom$n_ata # renomear os documentos (colunas) com o nome das atas

# Extrair os termos da matriz Termos x Documentos. Assim, tenho todos os termos utilizados na escrita da ata
terms = colnames(dtm)

# # Encontrar os termos positivos e negativos usando o Lexicon 
neg.terms = terms[terms %in% negative_words$x]
pos.terms = terms[terms %in% positive_words$x]

# Calcula os escores positivos e negativos das atas
neg.scores = rowSums(as.matrix(dtm[ , neg.terms])) # soma a quantidade de termos negativos nas linhas (cada documento da matriz) 
pos.scores = rowSums(as.matrix(dtm[ , pos.terms]))
total.words = rowSums(as.matrix(dtm))

# Calcula os escores das atas (document scores)
unigram = (pos.scores - neg.scores)/total.words

## 2) Bigram

print('Creating bigram score')

# Agrupa a frequencia de cada bigrama por ata 
bigrams <- pdf_link_copom %>%
  unnest_tokens(bigram, text, token = "ngrams", n = 2) %>%
  count(n_ata,bigram, sort = FALSE)  %>% arrange(desc(n))

# Calcula o Score de cada bigrama. Depois multiplica pela frequencia do bigrama no texto.
bigrams$w_score <- sapply(bigrams$bigram, polarity_score)
bigrams <- bigrams %>% mutate(score = n*w_score)

# Somar o Score final por ata
bigram_score_by_minute <- bigrams %>%
  group_by(n_ata) %>% 
  summarise(score = sum(score)) %>% 
  arrange((n_ata))

# Dividir o Score pela quantidade de palavras
bigram_score_by_minute$bigram <- bigram_score_by_minute$score/total.words

# Criando um único Dataframe contendo todos scores, atas e datas.


scores = data.frame( pdf_link_copom$n_ata,pdf_link_copom$pub_date, pdf_link_copom$real_date, 
                     unigram, bigram_score_by_minute$bigram , stringsAsFactors = FALSE)
colnames(scores) <- c("n_ata", "pub_date", "real_date", "unigram", "bigram")

plot(x = scores$real_date, y = scores$unigram, type = "l", xlab = "", ylab = "Score de Sentimento",main = "Unigram")
plot(x = scores$real_date, y = scores$bigram, type = "l", xlab = "", ylab = "Score de Sentimento",main = "Bigram")

