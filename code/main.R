
## Import libraries
library(pdftools)
library(lexicon)
library(tidyverse)
library(wordcloud)
library(Quandl)
library(rvest)
library(tm)
library(BETS)
library(dplyr)


## Get directory
root = rprojroot::is_rstudio_project 
mydir = root$find_file()

# Get functions
source(paste0(mydir,'/code/functions.R'))

# Apply web scraping
source(paste0(mydir,'/code/webscraping.R'))

# Download files
#download_pdf_all(pdf_link_copom, "/data/pdfs/") # Download all files
# download_pdf(pdf_link_copom, "/data/pdfs/",177) # Download a specific file

# Apply sentiment Analysis
source(paste0(mydir,'/code/sentiment_analysis.R'))

# Create Graph
source(paste0(mydir,'/code/graph.R'))

 