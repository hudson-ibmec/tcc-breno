# **README**

Pasta com dados utilizados e gerados na análise.

### **ESTRUTURA DESTA PASTA**

Nome do arquivo | Descrição
----------------|--------------------
[paper.Rmd](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/paper.Rmd) | Arquivo em Rmd contendo o Paper
[tcc_final.docx](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/paper/tcc_final.docx) | Word do TCC final
[tcc_final.pdf](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/paper/tcc_final.pdf) | PDF do TCC final
[**images**](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/paper/images) | Pasta com imagens utilizadas no Paper
[**references**](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/paper/references) | Pasta com referências utilizadas no Paper
[**template**](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/paper/template) | Pasta com templates utilizados no Paper
