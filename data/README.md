# **README**

Pasta com dados utilizados e gerados na análise.

### **ESTRUTURA DESTA PASTA**

Nome do arquivo | Descrição
----------------|--------------------
[**pdfs**](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/data/pdfs) | PDF's baixados pelo [webscraper](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/code/webscraping.R).
[ata_date.csv](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/data/ata_date.csv) | Datas de publicação das atas.
