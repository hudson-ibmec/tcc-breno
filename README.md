# **README**
Neste repositório você encontrará informações e códigos necessários para replicar o Trabalho de Conclusão de Curso (TCC) do aluno Breno Dolabela. Tal TCC está em formato de artigo conforme exigência do Ibmec - MG para o curso de Engenharia de Produção.  

### **RESUMO**
Neste trabalho, apresentamos conceitos e técnicas como o de Web Scraping, Text Mining e Processamento de Linguagem Natural, e as aplicamos na macroeconomia. A partir de um programa escrito na linguagem R, baixamos todas as Atas do Copom, documento que analisa a economia e é divulgado pelo Banco Central do Brasil. As palavras presentes nesse texto foram analisadas, e assim foi criado um índice posteriormente usado para explicar qual o sentimento dos responsáveis pelo documento em relação à economia. O índice foi comparado a indicadores macroeconômicos tendo sucesso em constatar uma relação entre as variáveis."


### **PRÉ-REQUISITOS**
O artigo foi gerado no [RStudio Cloud](https://rstudio.cloud/) por meio de um projeto que está disponível neste link. Nele você encontrará todos os códigos necessários para gerar o PDF do artigo bem como fazer as análises. Outra alternativa é clonar este repositório e executar o código em seu computador/notebook pessoal.

Para conseguir entender os códigos e replicar o artigo recomendamos que você faça os seguintes cursos na plataforma [DataCamp](https://www.datacamp.com/):

* [Working with the RStudio IDE (Part 1)](https://www.datacamp.com/courses/working-with-the-rstudio-ide-part-1)
* [Working with the RStudio IDE (Part 2)](https://www.datacamp.com/courses/working-with-the-rstudio-ide-part-2)
* [Introduction to R](https://www.datacamp.com/courses/free-introduction-to-r)
* [Intermediate R](https://www.datacamp.com/courses/intermediate-r)
* [Introduction to Shell for Data Sciente](https://www.datacamp.com/courses/introduction-to-shell-for-data-science)
* [Introduction to Git for Data Sciente](https://www.datacamp.com/courses/introduction-to-git-for-data-science)
* [Sentiment Analysis in R](https://www.datacamp.com/courses/sentiment-analysis-in-r)
* [Text Mining: Bag of Words](https://www.datacamp.com/courses/intro-to-text-mining-bag-of-words)

### **ESTRUTURA DE PASTAS DESTE REPOSITÓRIO**

Para facilitar o entendimento, dividimos os arquivos em:

Nome do arquivo | Pasta
----------------|--------------------
[**code**](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/code) | Códigos utilizados para fazer a análise.
[**data**](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/data) | Dados utilizados e gerados pelos códigos
[**paper**](https://gitlab.com/hudson-ibmec/tcc-breno/tree/master/paper) | Arquivos relativos à produção do paper
